<?php

/**
 * @file
 * Install, update, uninstall Requirements functions for the Kute module.
 */

define('KUTE_DOWNLOAD_URL', 'https://github.com/thednp/kute.js/archive/master.zip');

/**
 * Implements hook_requirements().
 */
function kute_requirements($phase) {
  if ($phase != 'runtime') {
    return [];
  }

  $requirements = [];

  // Check Kute.js library is exists.
  /** @var Drupal\Core\Asset\LibraryDiscovery $library_discovery */
  $library_discovery = \Drupal::service('library.discovery');
  $library_dist_kute = $library_discovery->getLibraryByName('kute', 'kute.js');
  $library_exists_js = FALSE;

  // Check if $library_dist_kute is an array before accessing its elements.
  if (is_array($library_dist_kute) && isset($library_dist_kute['js'][0]['data'])) {
    $library_exists_js = file_exists(DRUPAL_ROOT . '/' . $library_dist_kute['js'][0]['data']);
  }

  // Show the status of the library in the status report section.
  if ($library_exists_js) {
    $description = t('The Kute.js library was available in the local libraries path and enabled.');
  }
  else {
    $description = t('The Kute.js library is using <strong>CDN</strong> and is not installed in your local libraries.<br>You can <a href="@downloadUrl" rel="external" target="_blank">download</a> and extract to "/libraries/kute.js" then check file exists in your Drupal installation directory at the correct path "/libraries/kute.js/dist/kute.min.js".<br>See the Kute module README file for more details.', [
      '@downloadUrl' => KUTE_DOWNLOAD_URL,
    ]);
  }

  $requirements['kute'] = [
    'title'       => t('Kute.js library'),
    'value'       => $library_exists_js ? t('Installed') : t('Not installed'),
    'severity'    => $library_exists_js ? REQUIREMENT_OK : REQUIREMENT_ERROR,
    'description' => $description,
  ];

  return $requirements;
}

/**
 * Implements hook_install().
 */
function kute_install() {
  // Check for Kute.js library installation.
  $library_exists = kute_check_installed();
  if (!$library_exists) {
    \Drupal::messenger()->addWarning(t('The Kute module requires the Kute.js library.<br>Currently, The Kute.js is loaded via <strong>CDN</strong> and is not available in your local libraries.<br>Please <a href=":downloadUrl" rel="external" target="_blank">Download</a> and unzip into "/libraries/kute.js" directory.', [
      ':downloadUrl' => KUTE_DOWNLOAD_URL,
    ]));
  }

  \Drupal::messenger()->addMessage(t('Thanks for installing Kute module.'));
}
