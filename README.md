INTRODUCTION
------------

This module integrates the 'KUTE.js' library:
  - https://github.com/thednp/kute.js

KUTE.js is a powerful and popular JavaScript animation library.

A modern JavaScript animation engine built on ES6+ standards with
strong TypeScript definitions and most essential features for
the web with easy to use methods to set up high performance,
cross-browser animations. The focus is code quality, flexibility,
performance and size.


FEATURES
--------

KUTE.js library is now a fully-fledged animation engine that you
may use to construct complicated animations using properties or
components that cannot be animated with CSS3 transitions or other
animation engines, or characteristics that aren’t even written in
the standard yet.

 - Lightweight and fast
 - Offers a variety of features
 - Supports all major browsers
 - Plugin system
 - Responsive design
 - Disadvantages
 - Support for SVG


REQUIREMENTS
------------

Download 'KUTE.js' Library:
  - https://github.com/thednp/kute.js/archive/master.zip


INSTALLATION
------------

1. Download 'Kute' module archive
   - https://www.drupal.org/project/kute


2. Extract and place it in the root modules directory i.e.
   - /modules/kute (Or "/modules/contrib/kute")


3. Create a libraries directory in the root, if not already there i.e.
   - /libraries


4. Create a 'kute.js' directory inside it i.e.
   - /libraries/kute.js


5. Download 'Anime' library
   - https://github.com/thednp/kute.js/archive/master.zip


6. Place it in the /libraries/anime directory i.e.
   - /libraries/kute.js/dist/kute.min.js


7. Now, enable 'Kute' module.


USAGE
-----

Here is an KUTE.js example:

var tween1 = KUTE.fromTo('selector1',{rotate:0},{rotate:-720});

var tween2 = KUTE.fromTo('selector2',{rotateX:0},{rotateX:200});

var tween3 = KUTE.fromTo('selector3',{
    perspective:100,
    rotate3d:[0,0,0]
  },
  {
    perspective:100,
    rotate3d:[0,160,0]
  }
);

var tween4 = KUTE.fromTo('selector4',{rotateZ:0},{rotateZ:360});


GUIDE
=====

**Check official KUTE.js documentation for more information:**
  - https://thednp.github.io/kute.js/transformFunctions.html


How does it Work?
=================

1. Download KUTE.js library, Follow INSTALLATION in above.

2. Enable "Kute" module, Follow INSTALLATION in above.

3. Add script to your theme/module ja file, Follow USAGE in above.

4. Enjoy that.


MAINTAINERS
-----------

Current maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt

DEMO
-----------
http://thednp.github.io/kute.js
